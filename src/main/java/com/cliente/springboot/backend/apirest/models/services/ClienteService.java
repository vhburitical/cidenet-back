package com.cliente.springboot.backend.apirest.models.services;

import java.util.List;
import java.util.Optional;

import com.cliente.springboot.backend.apirest.models.entity.Cliente;

public interface ClienteService {
	public List<Cliente> findAll();
	public Cliente findById(Long id);
	public Cliente save(Cliente Cliente);
	public void delete (Long id);

}
