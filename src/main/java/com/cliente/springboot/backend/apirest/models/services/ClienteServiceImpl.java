package com.cliente.springboot.backend.apirest.models.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cliente.springboot.backend.apirest.models.dao.ClienteDao;
import com.cliente.springboot.backend.apirest.models.entity.Cliente;

@Service
public class ClienteServiceImpl implements ClienteService {
	@Autowired
	private ClienteDao clienteDao;
	
	@Transactional(readOnly = false)
	public List<Cliente> findAll() {
		// TODO Auto-generated method stub
		return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	@Transactional(readOnly = false)
	public Cliente findById(Long id) {
		// TODO Auto-generated method stub
		Cliente empleado = clienteDao.findById(id).orElse(null);
		return empleado;
	}

	@Override
	@Transactional(readOnly = false)
	public Cliente save(Cliente Cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(Cliente);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		clienteDao.deleteById(id);
		
	}
	

}
