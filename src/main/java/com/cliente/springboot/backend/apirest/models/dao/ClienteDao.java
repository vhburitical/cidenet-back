package com.cliente.springboot.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cliente.springboot.backend.apirest.models.entity.Cliente;

public interface ClienteDao extends JpaRepository<Cliente, Long> {

}
