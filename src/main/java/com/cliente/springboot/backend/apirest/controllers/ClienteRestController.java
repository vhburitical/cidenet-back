package com.cliente.springboot.backend.apirest.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cliente.springboot.backend.apirest.models.entity.Cliente;
import com.cliente.springboot.backend.apirest.models.services.ClienteService;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ClienteRestController {
	@Autowired
	private ClienteService clienteService;

	@GetMapping("/clientes")
	public List<Cliente> findAll() {
		return clienteService.findAll();
	}

	@GetMapping("/clientes/{id}")
	public Cliente show(@PathVariable Long id) {
		Cliente cliente = null;
		cliente = clienteService.findById(id);
		return cliente;
	}

	@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Cliente cliente, BindingResult result) {
		Cliente clienteNew = null;
		Cliente clienteCreate = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String frmtdDate = dateFormat.format(date);
			clienteNew = validateEmail(cliente);
			clienteNew.setRegistrationDate(frmtdDate);
			clienteNew.setStatus("ACTIVO");
			clienteCreate = clienteService.save(clienteNew);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ha sido creado con éxito!");
		response.put("cliente", clienteCreate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@RequestBody Cliente cliente, @PathVariable Long id, BindingResult result) {
		Cliente clienteNew = null;
		Cliente clienteActual = clienteService.findById(id);
		Cliente clienteUpdated = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		if (clienteActual == null) {
			response.put("mensaje", "Error: no se pudo editar, el cliente ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		try {
			clienteActual.setSurname(cliente.getSurname());
			clienteActual.setSecondSurname(cliente.getSecondSurname());
			clienteActual.setFirstName(cliente.getFirstName());
			clienteActual.setSecondName(cliente.getSecondName());
			clienteActual.setCountryEmployment(cliente.getCountryEmployment());
			clienteActual.setIdentificationType(cliente.getIdentificationType());
			clienteActual.setIdentificationNumber(cliente.getIdentificationNumber());
			clienteActual.setEmail(cliente.getEmail());
			clienteActual.setAdmissionDate(new Date());
			clienteActual.setArea(cliente.getArea());
			clienteActual.setStatus(cliente.getStatus());
			clienteNew = validateEmail(clienteActual);
			clienteUpdated = clienteService.save(clienteNew);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el cliente en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ha sido actualizado con éxito!");
		response.put("cliente", clienteUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			clienteService.delete(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar el cliente de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente eliminado con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	public Cliente validateEmail(Cliente empleado) {
		List<Cliente> validEmails = findAll();
		String email = empleado.getFirstName() + "." + empleado.getSurname() + "@cidenet.com.";
		if (empleado.getCountryEmployment().equalsIgnoreCase("colombia")) {
			email += "co";
		} else {
			email += "us";
		}
		int index = 0;
		email = email.replace("\\s", "");
		email = email.toLowerCase();
		String emailaux = email;

		for (Cliente mail : validEmails) {
			if (mail.getEmail().contains(email)) {
				email = emailaux.replace("@", "." + (++index) + "@");
			}
		}
		empleado.setEmail(email);
		empleado.setStatus("Activo");
		return empleado;
	}

}
